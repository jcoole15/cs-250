// Jeremiah Cooley
// Name validator extra credit assignment
// 1-25-18

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

bool validateName(string Name);

int main(int argc, char *argv[]) {

	ifstream in_file("./outfile.txt");
	if (!in_file) {
		cerr << "Failed to open input file" << endl;
	}
	else {
		string NameOutput;
		vector<string> Names;

		while (getline(in_file, NameOutput)) {
			Names.push_back(NameOutput);
		}

		in_file.close();

		for (size_t i = 0; i < Names.size(); i++){
			cout << Names.at(i) << endl;
		}
	}

	ofstream out_file("./outfile.txt", fstream::out | fstream::app);
	
	if (!out_file) {
		cerr << "Failed to open output file" << endl;
		exit(1);
	}

	string NameInput;

	while (true) {
		cout << "Enter a name to add to the list. Invalid names will be rejected. Enter -1 to quit." << endl;
		getline(cin, NameInput);
		if (NameInput == "-1") { break; }
		if (validateName(NameInput)) { out_file << NameInput << endl; }

	}

	out_file.close();

	cin.ignore();
	cin.get();
	return 0;
}

//Validates Name as being an ASCII letters/spaces only string
//Pre: Name is an initialized string
//Post: None
//Return: boolean indicating if Name is a valid "Name"
bool validateName(string Name) {
	char placeValue;
	for (size_t i = 0; i < Name.size(); i++) {
		placeValue = Name.at(i);
		if (!(placeValue >= 'a' && placeValue <= 'z' || placeValue >= 'A' && placeValue <= 'Z' || placeValue == ' ')) {
			return false;
		}
	}
	return true;
}