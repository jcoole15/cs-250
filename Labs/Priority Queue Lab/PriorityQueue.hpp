#ifndef _PRIORITY_QUEUE
#define _PRIORITY_QUEUE

#include <iostream>
#include <iomanip>
using namespace std;

#include "Menu.hpp"
#include "Job.hpp"

const int MAX_SIZE = 1000;

class PriorityQueue
{
public:
	PriorityQueue();
	void Push(Job* task);
	void Pop();
	Job* Front();
	int Size();
	void Display();

private:
	void ShiftRight(int atIndex);
	void ShiftLeft(int atIndex);

	Job* m_arr[MAX_SIZE];
	int m_itemCount;
};

PriorityQueue::PriorityQueue()
{
	m_itemCount = 0;
}

void PriorityQueue::Push(Job* task)
{
	if (m_itemCount == MAX_SIZE) {
		throw runtime_error("Queue is Full.");
	}
	int targetIndex = m_itemCount;
	for (size_t i = 0; i < m_itemCount; i++) {
		if (task->dueOnDay < m_arr[i]->dueOnDay) {
			targetIndex = i;
			break;
		}
	}

	ShiftRight(targetIndex);
	m_arr[targetIndex] = task;
	m_itemCount++;
	return;
}

void PriorityQueue::Pop()
{
	if (m_itemCount == 0) {
		throw runtime_error("Can't Pop empty queue");
	}
	ShiftLeft(0);
	m_itemCount--;
}

Job* PriorityQueue::Front()
{
	return m_arr[0];
}

void PriorityQueue::ShiftRight(int atIndex)
{
	for (int i = m_itemCount; i > atIndex; i--)
	{
		m_arr[i] = m_arr[i - 1];
	}
}

void PriorityQueue::ShiftLeft(int atIndex)
{
	for (int i = atIndex + 1; i < m_itemCount; i++)
	{
		m_arr[i - 1] = m_arr[i];
	}
}

int PriorityQueue::Size()
{
	return m_itemCount;
}

void PriorityQueue::Display()
{
	cout << endl;
	Menu::Header("Priority Queue");
	cout << left
		<< setw(10) << "INDEX"
		<< setw(10) << "DUE"
		<< setw(50) << "ASSIGNMENT" << endl;

	for (int i = 0; i < m_itemCount; i++)
	{
		cout << left
			<< setw(10) << i
			<< setw(10) << m_arr[i]->dueOnDay
			<< setw(50) << m_arr[i]->assignment << endl;
	}
}

#endif
