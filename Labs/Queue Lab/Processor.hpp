#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
public:
	void FirstComeFirstServe(vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile);
	void RoundRobin(vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile);
};

void Processor::FirstComeFirstServe(vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile)
{
	ofstream out_f = ofstream(logFile);
	out_f << "First Come First Served (FCFS)" << endl;
	int cycleCount = 0;
	size_t queueSize = jobQueue.Size();
	for (size_t i = 0; i < queueSize; i++) {
		out_f << "Processing Job #" << i << endl;
		Job * currentJob = jobQueue.Front();
		while (!currentJob->fcfs_done) {
			currentJob->Work(FCFS);
			out_f << "Cycle: " << '\t' << cycleCount << '\t' << "Remaining: " << '\t' << currentJob->fcfs_timeRemaining << endl;
			cycleCount++;
		}
		currentJob->SetFinishTime(cycleCount, FCFS);
		jobQueue.Pop();
	}

	out_f << endl << "ID\tTime" << endl;

	int sumTime = cycleCount + 1;
	for (size_t j = 0; j < queueSize; j++) {
		Job statJob = allJobs.at(j);
		out_f << statJob.id << '\t' << statJob.fcfs_finishTime << endl;
	}
	int avgTime = sumTime / queueSize;
	out_f << "Total Time: " << sumTime << endl;
	out_f << "Avg Time: " << avgTime << endl;
	out_f.close();
}

void Processor::RoundRobin(vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile)
{
	ofstream out_f = ofstream(logFile);
	out_f << "Round-Robin (RR)" << endl;
	int cycleCount = 0;
	while (!jobQueue.IsEmpty()) {
		Job * currentJob = jobQueue.Front();
		jobQueue.Pop();
		out_f << "Processing Job: " << currentJob->id << endl;
		for (int i = 0; i < timePerProcess; i++) {
			currentJob->Work(RR);
			out_f << "CYCLE " << cycleCount << "\tREMAINING " << currentJob->rr_timeRemaining << endl;
			cycleCount++;
			if (currentJob->rr_done) {
				currentJob->SetFinishTime(cycleCount, RR);
				break;
			}
		}
		if (!currentJob->rr_done) {
			currentJob->rr_timesInterrupted++;
			jobQueue.Push(currentJob);
		}
	}


	size_t queueSize = allJobs.size();
	int sumTime = cycleCount + 1;
	int avgTime = 0;
	out_f << "Round Robin Results" << endl;
	out_f << "ID\tTime to Complete\tTimes Interrupted" << endl;
	for (size_t j = 0; j < queueSize; j++) {
		Job statJob = allJobs.at(j);
		out_f << statJob.id << '\t' << statJob.rr_finishTime << '\t' << statJob.rr_timesInterrupted << endl;
		avgTime += statJob.rr_finishTime;
	}
	avgTime /=  queueSize;

	out_f << "Total time: " << sumTime << endl;
	out_f << "Avg time: " << avgTime << endl;
	out_f << "Round robin interval: " << timePerProcess << endl;
	out_f.close();
}

#endif
