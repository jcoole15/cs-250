#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "LinkedList.hpp"

template <typename T>
class Queue : LinkedList<T>
{
    public:
    void Push( T data )
    {
		LinkedList<T>::PushBack(data); // placeholder
    }

    void Pop()
    {
        LinkedList<T>::PopFront(); // placeholder
    }

    T& Front()
    {
		return LinkedList<T>::GetFirst(); // placeholder
    }

    int Size()
    {
        return LinkedList<T>::Size(); // placeholder
    }

	bool IsEmpty() {
		return LinkedList<T>::IsEmpty();
	}

    private:
};

#endif
