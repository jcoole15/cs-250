#ifndef _function1
#define _function1

/*
CountUp functions
@param int start    The starting value (inclusive) to begin at
@param int end      The end value (inclusive) to run until

Display the numbers between [start] and [end], incrementing by 1 each time.
*/

void CountUp_Iter( int start, int end )
{
	for (size_t i = start; i <= end; i++) {
		cout << i << '\t';
	}
	cout << endl;
}

void CountUp_Rec( int start, int end )
{
	cout << start << '\t';
	if (start == end) { return; }
	CountUp_Rec(start + 1, end);
}

#endif
