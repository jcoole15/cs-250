#ifndef _function3
#define _function3

/*
Factorial functions
@param int n        The n! value

Calculate n! by multiplying n * (n-1) * (n-2) * ... * 3 * 2 * 1.
*/

int Factorial_Iter( int n )
{
	int result = 1;
	for (int i = 1; i <= n; i++) {
		result *= i;
	}
	return result;
}

int Factorial_Rec( int n )
{
	if (n == 1 || n == 0) { return 1; }
	return n * Factorial_Rec(n - 1);
}

#endif
