// Lab - Standard Template Library - Part 4 - Stacks
// Jeremiah Cooley

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> letters;
	string input;
	while (true) {
		cout << "Enter letter(s) of a word, or UNDO to remove the last entry, or DONE if you are finished." << endl;
		cin >> input;
		if (input == "UNDO") { letters.pop(); continue; }
		if (input == "DONE") { break; }
		letters.push(input);
	}
	cout << "Finished phrase: " << endl;
	while (!letters.empty()) {
		cout << letters.top();
		letters.pop();
	}
	cout << endl;
    cin.ignore();
    cin.get();
    return 0;
}
