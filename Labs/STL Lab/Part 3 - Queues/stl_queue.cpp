// Lab - Standard Template Library - Part 3 - Queues
// Jeremiah Cooley

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;
	double transaction;

	while (true) {
		cout << "Enter a transaction amount, or any non number to stop." << endl;
		while (cin >> transaction) {
			transactions.push(transaction);
		}
		break;
	}
	float balance = 0.0;
	while (!transactions.empty()) {
		cout << "Current transaction in processing: " << transactions.front() << endl;
		balance += transactions.front();
		transactions.pop();
	}
	cout << "Ending balance is: " << balance << endl;

    cin.ignore();
	cin.get();
    return 0;
}
