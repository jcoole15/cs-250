// Lab - Standard Template Library - Part 1 - Vectors
// Jeremiah Cooley

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	int input;
	vector<string> courses;
	string courseName;

	bool done = false;
	while (!done) {
		cout << "Vector Size: " << courses.size() << endl;
		cout << "Enter a number for an option" << endl;
		cout << "1.Add a new course" << endl;
		cout << "2.Remove the last course" << endl;
		cout << "3.Display course list" << endl;
		cout << "4. Quit" << endl;
		cin >> input;
		switch (input) {
		case 1:
			cout << "Enter a course name." << endl;			
			cin >> courseName;
			courses.push_back(courseName);
			break;
		case 2:
			courses.pop_back();
			break;
		case 3:
			cout << "Course list: " << endl;
			for (size_t i = 0; i < courses.size(); i++) {
				cout << courses[i] << endl;
			}
			break;
		case 4:
			done = true;
			break;
		}
	}

	cin.ignore();
	cin.get();
	return 0;
}
