// Lab - Standard Template Library - Part 2 - Lists
// Jeremiah Cooley

#include <iostream>
#include <list>
#include <string>
using namespace std;

void DisplayList(list<string>& states);

int main()
{
	list<string> states;
	string stateName;
	char input;
	while (true) {
		cout << "Enter a number to choose an option" << endl;
		cout << "1.Add a new state to the front of the list" << endl;
		cout << "2.Add a new state to the back of the list" << endl;
		cout << "3.Pop a state from the front of the list" << endl;
		cout << "4.Pop a state from the end of the list" << endl;
		cout << "5.Continue" << endl;
		cin >> input;
		if (input == '5') { break; }
		switch (input) {
		case '1':
			cout << "Enter a state name" << endl;
			cin >> stateName;
			states.push_front(stateName);
			break;
		case '2':
			cout << "Enter a state name" << endl;
			cin >> stateName;
			states.push_front(stateName);
			break;
		case '3':
			states.pop_front();
			break;
		case '4':
			states.pop_back();
			break;
		default:
			cout << "Invalid option selected" << endl;
			continue;
		}
	}

	cout << "Original List" << endl;
	DisplayList(states);
	cout << "Reversed List" << endl;
	states.reverse();
	DisplayList(states);
	states.reverse();
	states.sort();
	cout << "Sorted Original List" << endl;	
	DisplayList(states);
	states.reverse();
	cout << "Sorted Reversed List" << endl;	
	DisplayList(states);
    cin.ignore();
    cin.get();
    return 0;
}

void DisplayList(list<string>& states) {
	for (list<string>::iterator i = states.begin(); i != states.end(); i++) {
		cout << *i << '\t';
	}
	cout << endl;
}