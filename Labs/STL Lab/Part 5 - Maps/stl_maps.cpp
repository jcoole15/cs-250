// Lab - Standard Template Library - Part 5 - Maps
// Jeremiah Cooley

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	map<char, string> colors;
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";

	char input;
	while (true) {
		cout << "Enter a letter." << endl;
		cin >> input;
		if (input == 'q') { break; }
		cout << "Hex: " << colors[input] << endl;
	}

    cin.ignore();
    cin.get();
    return 0;
}
