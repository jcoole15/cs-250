// Jeremiah Cooley
// Project 1
// List/fixed array implemenation
#ifndef _LIST_HPP
#define _LIST_HPP

const int ARRAY_SIZE = 100;

template <typename T>
class List
{
private:
	// private member variables
	int m_itemCount;
	T m_arr[ARRAY_SIZE];

	// functions for internal-workings
	bool ShiftRight(int atIndex)
	{
		if (atIndex < 0 || atIndex >= Size()) {
			return false;
		}

		for (size_t i = Size(); i > atIndex; i--) {
			m_arr[i] = m_arr[i - 1];
		}
		return true;
	}

	bool ShiftLeft(int atIndex)
	{

		if (atIndex < 0 || atIndex >= Size()) {
			return false;
		}

		for (size_t i = atIndex; i < Size(); i++) {
			m_arr[i] = m_arr[i + 1];
		}

		return true;
	}

public:
	List() : m_itemCount(0)
	{

	}

	~List()
	{
	}

	// Core functionality
	int     Size() const
	{
		return m_itemCount; // placeholder
	}

	bool    IsEmpty() const
	{
		return m_itemCount == 0; // placeholder
	}

	bool    IsFull() const
	{
		return m_itemCount == ARRAY_SIZE; // placeholder
	}

	bool    PushFront(const T& newItem)
	{
		if (IsFull()) {
			return false;
		}

		Insert(0, newItem);
		return true;
	}

	bool    PushBack(const T& newItem)
	{
		if (IsFull()) {
			return false;
		}

		return Insert(Size(), newItem);

	}

	bool    Insert(int atIndex, const T& item)
	{
		if (IsFull()) {
			return false;
		}

		if (atIndex < 0 || atIndex > Size()) {
			return false;
		}

		if (atIndex == Size()) {
			m_arr[atIndex] = item;
		}
		else {
			ShiftRight(atIndex);
			m_arr[atIndex] = item;
		}

		m_itemCount++;
		return true;

	}

	bool    PopFront()
	{
		if (IsEmpty()) {
			return false;
		}

		ShiftLeft(0);
		m_itemCount--;
		return true;
	}

	bool    PopBack()
	{
		if (IsEmpty()) {
			return false;
		}

		m_itemCount--;
		return true;
	}

	bool    Remove(const T& item)
	{
		if (IsEmpty()) {
			return false;
		}

		for (size_t i = 0; i < Size(); i++) {
			if (m_arr[i] == item) {
				ShiftLeft(i);
				m_itemCount--;
			}
		}

		return true;
	}

	bool    RemoveAt(int atIndex)
	{
		if (IsEmpty()) {
			return false;
		}

		if (atIndex < 0 || atIndex >= Size()) {
			return false;
		}

		ShiftLeft(atIndex);
		m_itemCount--;
		return true;
	}

	void    Clear()
	{
		m_itemCount = 0;
	}

	// Accessors
	T*      Get(int atIndex)
	{
		if (IsEmpty()) {
			return nullptr;
		}

		if (atIndex < 0 || atIndex >= Size()) {
			return nullptr;
		}

		return &(m_arr[atIndex]);
	}

	T*      GetFront()
	{
		if (IsEmpty()) {
			return nullptr;
		}

		return Get(0);
	}

	T*      GetBack()
	{
		if (IsEmpty()) {
			return nullptr;
		}

		return Get(Size() - 1);
	}

	// Additional functionality
	int     GetCountOf(const T& item) const
	{
		if (IsEmpty()) {
			return 0;
		}
		int count = 0;
		for (size_t i = 0; i < Size(); i++) {
			if (m_arr[i] == item) {
				count++;
			}
		}
		return count;
	}

	bool    Contains(const T& item) const
	{
		if (IsEmpty()) {
			return false;
		}

		for (size_t i = 0; i < Size(); i++) {
			if (m_arr[i] == item) {
				return true;
			}
		}

		return false;
	}

	friend class Tester;
};


#endif
