// Jeremiah Cooley
// Project 1
// Test Implementation
#include "Tester.hpp"

void Tester::RunTests()
{
	Test_Init();
	Test_IsEmpty();
	Test_IsFull();
	Test_Size();
	Test_GetCountOf();
	Test_Contains();

	Test_PushFront();
	Test_PushBack();

	Test_Get();
	Test_GetFront();
	Test_GetBack();

	Test_PopFront();
	Test_PopBack();
	Test_Clear();

	Test_ShiftRight();
	Test_ShiftLeft();

	Test_Remove();
	Test_Insert();
}

void Tester::DrawLine()
{
	cout << endl;
	for (int i = 0; i < 80; i++)
	{
		cout << "-";
	}
	cout << endl;
}

void Tester::Test_Init()
{
	DrawLine();
	cout << "TEST: Test_Init" << endl;

	{//Test Begin 1
		cout << endl << "Test 1 List constructor" << endl;
		List<int> testList = List<int>();
		int expected = 0;
		int actual = testList.Size();

		cout << "Expected Size: " << expected << endl;
		cout << "Actual Size: " << actual << endl;

		if (expected == actual) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}//Test End
}

void Tester::Test_ShiftRight()
{
	DrawLine();
	cout << "TEST: Test_ShiftRight" << endl;

	{//Test Begin 1
		List<int> testList = List<int>();
		testList.PushBack(1);
		testList.PushBack(2);
		testList.PushBack(3);

		cout << "Testing ShiftRight outside of bounds" << endl;
		cout << "Result " << (testList.ShiftRight(4) ? "Fail" : "Pass") << endl;
	}//Test End

	{//Test Begin 2
		bool Pass = true;
		List<int> testList = List<int>();
		testList.PushBack(1);
		testList.PushBack(2);
		testList.PushBack(3);

		bool didShift;
		cout << "Testing ShiftRight in bounds" << endl;
		didShift = testList.ShiftRight(1);
		if (!didShift) {
			cout << "Fail: Did not Shift" << endl;
			Pass = false;
		}

		int expectedAtShiftIndex = 2;
		int *actualAtShiftIndex = testList.Get(1);
		if (actualAtShiftIndex == nullptr) {
			cout << "Got nullptr for Get TEST FAIL" << endl;
			exit(1);
		}
		if (expectedAtShiftIndex != *actualAtShiftIndex) {
			cout << "Fail: Did not shift properly" << endl;
			Pass = false;
		}
		if (Pass) {
			cout << "Pass!" << endl;
		}
	}//Test End
}

void Tester::Test_ShiftLeft()
{
	DrawLine();
	cout << "TEST: Test_ShiftLeft" << endl;

	{//Test Begin 1
		List<int> testList = List<int>();
		testList.PushBack(1);
		testList.PushBack(2);
		testList.PushBack(3);

		cout << "Testing ShiftLeft outside of bounds" << endl;
		cout << "Result" << (testList.ShiftLeft(4) ? "Fail" : "Pass") << endl;
	}//Test End

	{//Test Begin 2
		bool Pass = true;
		List<int> testList = List<int>();
		testList.PushBack(1);
		testList.PushBack(2);
		testList.PushBack(3);

		bool didShift;
		cout << "Testing ShiftLeft in bounds" << endl;
		didShift = testList.ShiftLeft(1);
		if (!didShift) {
			cout << "Fail: Did not Shift" << endl;
			Pass = false;
		}

		int expectedAtShiftIndex = 3;
		int* actualAtShiftIndex = testList.Get(1);
		if (actualAtShiftIndex == nullptr) {
			cout << "Got nullptr for Get TEST FAIL" << endl;
			exit(1);
		}
		if (expectedAtShiftIndex != *actualAtShiftIndex) {
			cout << "Fail: Did not shift properly" << endl;
			Pass = false;
		}
		if (Pass) {
			cout << "Pass!" << endl;
		}
	}//Test End
}

void Tester::Test_Size()
{
	DrawLine();
	cout << "TEST: Test_Size" << endl;

	{   // Test begin 1
		cout << endl << "Test 1 Size of empty list" << endl;
		List<int> testList;
		int expectedSize = 0;
		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{   // Test begin 2
		cout << endl << "Test 2 size of partially filled list" << endl;
		List<int> testList;
		size_t expectedSize = 43;

		for (size_t i = 0; i < expectedSize; i++) {
			testList.PushBack(rand());
		}

		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}   // Test end

	{	// Test begin 3
		cout << endl << "Test 3 size of overfilled list" << endl;
		List<int> testList;
		int expectedSize = 100;
		for (size_t i = 0; i < 102; i++) {
			testList.PushBack(rand());
		}

		int actualSize = testList.Size();

		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}	// Test end 3

	{	// Test begin 4
		cout << endl << "Test 4 size of list after adding and removing elements" << endl;
		List<int> testList;
		int expectedSize = 5;
		for (size_t i = 0; i < 7; i++) {
			testList.PushBack(rand());
		}
		testList.PopBack();
		testList.PopBack();

		int actualSize = testList.Size();
		cout << "Expected size: " << expectedSize << endl;
		cout << "Actual size:   " << actualSize << endl;

		if (actualSize == expectedSize)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}	// Test begin 4
}

void Tester::Test_IsEmpty()
{
	DrawLine();
	cout << "TEST: Test_IsEmpty" << endl;

	{	//Test begin 1
		cout << endl << "Test 1 IsEmpty on newly created list" << endl;
		List<int> testList;
		bool expectedResult = true;
		bool actualResult = testList.IsEmpty();

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test 2 IsEmpty on list with elements added" << endl;
		List<int> testList;
		testList.PushBack(rand());
		bool expectedResult = false;
		bool actualResult = testList.IsEmpty();

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}	//Test end 2
}

void Tester::Test_IsFull()
{
	DrawLine();
	cout << "TEST: Test_IsFull" << endl;

	{	//Test begin 1
		cout << endl << "Test 1 IsFull on newly created list." << endl;
		List<int> testList;
		bool expectedResult = false;
		bool actualResult = testList.IsFull();

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test 2 IsFull on full list." << endl;
		List<int> testList;
		bool expectedResult = true;
		for (size_t i = 0; i < 102; i++) {
			testList.PushBack(rand());
		}

		bool actualResult = testList.Size();

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}	//Test end 2

	{	//Test begin 3
		cout << endl << "Test 3 IsFull on partially filled list." << endl;
		List<int> testList;
		bool expectedResult = false;
		bool actualResult = testList.Size();
		testList.PushBack(rand());

		if (actualResult == expectedResult)
		{
			cout << "Pass" << endl;
		}
		else
		{
			cout << "Fail" << endl;
		}
	}	//Test end 3

}

void Tester::Test_PushFront()
{
	DrawLine();
	cout << "TEST: Test_PushFront" << endl;

	{	//Test begin 1
		cout << endl << "Test PushFront 1 Item" << endl;
		List<int> testList;
		int testItem = rand();
		bool actualReturn = testList.PushFront(testItem);
		bool expectedReturn = true;
		int actualSize = testList.Size();
		int expectedSize = 1;
		int *actualFront = testList.GetFront();
		int expectedFront = testItem;

		if (actualReturn == expectedReturn) {
			cout << "Return value Pass" << endl;
		}
		else {
			cout << "Return value Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size value Pass" << endl;
		}
		else {
			cout << "Size value Fail" << endl;
		}

		if (actualFront == nullptr) {
			cout << "Got nullptr for front TEST FAIL" << endl;
			exit(1);
		}
		if (*actualFront == expectedFront) {
			cout << "GetFront value Pass" << endl;
		}
		else {
			cout << "GetFront value Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test PushFront 100 Items" << endl;
		List<int> testList;
		bool actualReturn;
		for (size_t i = 0; i < 102; i++) {
			actualReturn = testList.PushFront(rand());
		}
		bool expectedReturn = false;
		int actualSize = testList.Size();
		int expectedSize = 100;

		if (actualReturn == expectedReturn) {
			cout << "Return value Pass" << endl;
		}
		else {
			cout << "Return value Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size value Pass" << endl;
		}
		else {
			cout << "Size value Fail" << endl;
		}
	}	//Test end 2
}

void Tester::Test_PushBack()
{
	DrawLine();
	cout << "TEST: Test_PushBack" << endl;

	{	//Test begin 1
		cout << endl << "Test PushBack 1 Item" << endl;
		List<int> testList;
		int testItem = rand();
		bool actualReturn = testList.PushBack(testItem);
		bool expectedReturn = true;
		int actualSize = testList.Size();
		int expectedSize = 1;
		int *actualBack = testList.GetBack();
		int expectedBack = testItem;

		if (actualReturn == expectedReturn) {
			cout << "Return value Pass" << endl;
		}
		else {
			cout << "Return value Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size value Pass" << endl;
		}
		else {
			cout << "Size value Fail" << endl;
		}

		if (actualBack == nullptr) {
			cout << "Got nullptr for Back TEST FAIL" << endl;
			exit(1);
		}
		if (*actualBack == expectedBack) {
			cout << "GetBack value Pass" << endl;
		}
		else {
			cout << "GetBack value Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test PushBack 100 Items" << endl;
		List<int> testList;
		bool actualReturn;
		for (size_t i = 0; i < 101; i++) {
			actualReturn = testList.PushBack(rand());
		}
		bool expectedReturn = false;
		int actualSize = testList.Size();
		int expectedSize = 100;

		if (actualReturn == expectedReturn) {
			cout << "Return value Pass" << endl;
		}
		else {
			cout << "Return value Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size value Pass" << endl;
		}
		else {
			cout << "Size value Fail" << endl;
		}
	}	//Test end 2
}

void Tester::Test_PopFront()
{
	DrawLine();
	cout << "TEST: Test_PopFront" << endl;

	{	//Begin test 1
		cout << endl << "Test PopFront on empty list." << endl;
		List<int> testList;
		bool actualReturn = testList.PopFront();
		bool expectedReturn = false;

		if (actualReturn == expectedReturn) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//End test 1

	{	//Begin test 2
		cout << endl << "Test PopFront on List with 2 items" << endl;
		int testItem1 = 2, testItem2 = 3;
		List<int> testList;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		bool actualReturn = testList.PopFront();
		bool expectedReturn = true;
		int *actualFront = testList.GetFront();
		int expectedFront = testItem2;
		int actualSize = testList.Size();
		int expectedSize = 1;

		if (actualReturn == expectedReturn) {
			cout << "Return value Pass" << endl;
		}
		else {
			cout << "Return value Fail" << endl;
		}

		if (actualFront == nullptr) {
			cout << "Got nullptr for Front TEST FAIL" << endl;
		}

		if (*actualFront == expectedFront) {
			cout << "Front value Pass" << endl;
		}
		else {
			cout << "Front value Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size value Pass" << endl;
		}
		else {
			cout << "Size value Fail" << endl;
		}
	}	//End test 2
}

void Tester::Test_PopBack()
{
	DrawLine();
	cout << "TEST: Test_PopBack" << endl;

	{	//Begin test 1
		cout << endl << "Test PopBack on empty list." << endl;
		List<int> testList;
		bool actualReturn = testList.PopBack();
		bool expectedReturn = false;

		if (actualReturn == expectedReturn) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//End test 1

	{	//Begin test 2
		cout << endl << "Test PopBack on List with 2 items" << endl;
		int testItem1 = 4, testItem2 = 5;
		List<int> testList;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		bool actualReturn = testList.PopBack();
		bool expectedReturn = true;
		int *actualBack = testList.GetBack();
		int expectedBack = testItem1;
		int actualSize = testList.Size();
		int expectedSize = 1;

		if (actualReturn == expectedReturn) {
			cout << "Return value Pass" << endl;
		}
		else {
			cout << "Return value Fail" << endl;
		}

		if (actualBack == nullptr) {
			cout << "Got nullptr for Back TEST FAIL" << endl;
		}

		if (*actualBack == expectedBack) {
			cout << "Front value Pass" << endl;
		}
		else {
			cout << "Front value fFail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size value Pass" << endl;
		}
		else {
			cout << "Size value Fail" << endl;
		}
	}	//End test 2
}

void Tester::Test_Clear()
{
	DrawLine();
	cout << "TEST: Test_Clear" << endl;

	{	//Test begin 1
		cout << endl << "Test Clear" << endl;
		List<int> testList;
		for (int i = 0; i < 3; i++) {
			testList.PushBack(rand());
		}

		testList.Clear();
		int expectedSize = 0;
		int actualSize = testList.Size();

		if (expectedSize == actualSize) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 1
}

void Tester::Test_Get()
{
	DrawLine();
	cout << "TEST: Test_Get" << endl;

	{	//Test begin 1
		cout << endl << "Test Get with item not in list" << endl;
		List<int> testList;
		for (int i = 0; i < 10; i++) {
			testList.PushBack(i);
		}
		int testIndex = 20;
		int* actualReturn = testList.Get(testIndex);
		if (actualReturn == nullptr) {
			cout << "Pass" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test Get with item in list" << endl;
		List<int> testList;
		for (int i = 0; i < 10; i++) {
			testList.PushBack(i);
		}
		int expectedValue = 5;
		int *actualValue = testList.Get(expectedValue);
		if (actualValue == nullptr) {
			cout << "TEST FAIL" << endl;
			exit(1);
		}
		if (*actualValue == expectedValue) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 2
}

void Tester::Test_GetFront()
{
	DrawLine();
	cout << "TEST: Test_GetFront" << endl;

	{	//Test begin 1
		cout << endl << "Test GetFront on empty list" << endl;
		List<int> testList;
		int *actualValue = testList.GetFront();
		if (actualValue == nullptr) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test GetFront on non-empty list" << endl;
		List<int> testList;
		int testItem = 1;
		testList.PushFront(testItem);
		int *actualValue = testList.GetFront();

		if (actualValue == nullptr) {
			cout << "Got nullptr for GetFront TEST FAIL" << endl;
			exit(1);
		}
		if (*actualValue == testItem) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 2
}

void Tester::Test_GetBack()
{
	DrawLine();
	cout << "TEST: Test_GetBack" << endl;

	{	//Test begin 1
		cout << endl << "Test GetBack on empty list" << endl;
		List<int> testList;
		int *actualValue = testList.GetBack();
		if (actualValue == nullptr) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test GetBack on non-empty list" << endl;
		List<int> testList;
		int testItem1 = 2, testItem2 = 3;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);

		int *actualValue = testList.GetBack();

		if (actualValue == nullptr) {
			cout << "Got nullptr for GetBack TEST FAIL" << endl;
			exit(1);
		}
		if (*actualValue == testItem2) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 2
}

void Tester::Test_GetCountOf()
{
	DrawLine();
	cout << "TEST: Test_GetCountOf" << endl;

	{	//Begin test 1
		cout << endl << "Test GetCountOf with 2 identical items in list" << endl;
		List<int> testList;
		int testItem = 4;
		testList.PushBack(testItem);
		testList.PushBack(testItem);
		int actualValue = testList.GetCountOf(testItem);
		int expectedValue = 2;

		cout << "Actual count: " << actualValue << endl;
		cout << "Expected count: " << expectedValue << endl;

		if (actualValue == expectedValue) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}

	}	//End test 1

	{	//Begin test 2
		cout << endl << "Test GetCountOf with 2 different items in list" << endl;
		List<int> testList;
		int testItem1 = 5, testItem2 = 6;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		int actualValue = testList.GetCountOf(testItem1);
		int expectedValue = 1;

		cout << "Actual count: " << actualValue << endl;
		cout << "Expected count: " << expectedValue << endl;

		if (actualValue == expectedValue) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}

	}	//End test 2

	{	//Begin test 3
		cout << endl << "Test GetCountOf with target value not in list" << endl;
		List<int> testList;
		int testItem1 = 10, testItem2 = 29;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		int targetValue = 30;
		int actualValue = testList.GetCountOf(targetValue);
		int expectedValue = 0;

		cout << "Actual count: " << actualValue << endl;
		cout << "Expected count: " << expectedValue << endl;

		if (actualValue == expectedValue) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//End test 3
}

void Tester::Test_Contains()
{
	DrawLine();
	cout << "TEST: Test_Contains" << endl;

	{	//Test begin 1
		cout << endl << "Test Contains with item in list" << endl;
		List<int> testList;
		int testItem1 = 1, testItem2 = 3;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		bool actualValue = testList.Contains(testItem1);
		bool expectedValue = true;

		if (actualValue == expectedValue) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test Contains with item not in list" << endl;
		List<int> testList;
		int testItem1 = 1, testItem2 = 3;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		int targetValue = 8;
		bool actualValue = testList.Contains(targetValue);
		bool expectedValue = false;

		if (actualValue == expectedValue) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 2
}

void Tester::Test_Remove()
{
	DrawLine();
	cout << "TEST: Test_Remove" << endl;

	{	//Test begin 1
		cout << endl << "Testing remove with value not in list." << endl;
		List<int> testList;
		int testItem1 = 2, testItem2 = 3, testItem3 = 4;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		testList.PushBack(testItem3);
		int targetValue = 8;
		bool actualValue = testList.Remove(targetValue);
		bool expectedValue = true;

		if (actualValue == expectedValue) {
			cout << "Pass" << endl;
		}
		else {
			cout << "Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Testing remove with value in list." << endl;
		List<int> testList;
		int testItem1 = 8, testItem2 = 139, testItem3 = 2983, testItem4 = 283;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		testList.PushBack(testItem3);
		testList.PushBack(testItem4);
		bool actualValue = testList.Remove(testItem1);
		bool expectedValue = true;
		int actualSize = testList.Size();
		int expectedSize = 3;
		int actualCount = testList.GetCountOf(testItem1);
		int expectedCount = 0;

		if (actualValue == expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}

		if (actualCount == expectedCount) {
			cout << "Count test Pass" << endl;
		}
		else {
			cout << "Count test Fail" << endl;
		}
	}	//Test end 2

}

void Tester::Test_RemoveAt()
{
	DrawLine();
	cout << "TEST: Test_RemoveAt" << endl;

	{	//Test begin 1
		cout << endl << "Test RemoveAt index out of list" << endl;
		List<int> testList;
		int testItem1 = 2, testItem2 = 3, testItem3 = 4;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		testList.PushBack(testItem3);
		int targetIndex = 8;
		bool actualValue = testList.RemoveAt(targetIndex);
		bool expectedValue = false;
		int actualSize = testList.Size();
		int expectedSize = 3;

		if (actualValue = expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}
	}	//Test end 1

	{	//Test begin 2
		cout << endl << "Test RemoveAt index in list" << endl;
		List<int> testList;
		int testItem1 = 2, testItem2 = 3, testItem3 = 4;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		testList.PushBack(testItem3);
		int targetIndex = 2;
		bool actualValue = testList.RemoveAt(targetIndex);
		bool expectedValue = true;
		int actualSize = testList.Size();
		int expectedSize = 2;

		if (actualValue = expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}
	}	//Test end 2

	{	//Test begin 3
		cout << endl << "Test RemoveAt on empty list" << endl;
		List<int> testList;
		int targetIndex = 0;
		bool actualValue = testList.RemoveAt(targetIndex);
		bool expectedValue = false;
		int actualSize = testList.Size();
		int expectedSize = 0;

		if (actualValue == expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}
	}	//Test end 3
}

void Tester::Test_Insert()
{
	DrawLine();
	cout << "TEST: Test_Insert" << endl;

	{	//Test begin 1
		cout << endl << "Test Insert into empty list" << endl;
		List<int> testList;
		int testItem = 2;
		bool actualValue = testList.Insert(0, testItem);
		bool expectedValue = true;
		int actualSize = testList.Size();
		int expectedSize = 1;

		if (actualValue == expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}

	}	//Test end 1
		
	{	//Test begin 2
		cout << endl << "Test Insert into full list" << endl;
		List<int> testList;
		for (size_t i = 0; i < 101; i++) {
			testList.PushBack(rand());
		}
		int testItem = 2;
		bool actualValue = testList.Insert(0, testItem);
		bool expectedValue = false;
		int actualSize = testList.Size();
		int expectedSize = 100;

		if (actualValue == expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}
	}	//Test end 2

	{	//Test begin 3
		cout << endl << "Test Insert into empty list with invalid index" << endl;
		List<int> testList;
		int testItem = 2;
		bool actualValue = testList.Insert(5, testItem);
		bool expectedValue = false;
		int actualSize = testList.Size();
		int expectedSize = 0;

		if (actualValue == expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}
	}	//Test end 3

	{	//Test begin 4
		cout << endl << "Test Insert into front of list" << endl;
		List<int> testList;
		int testItem1 = 2,testItem2= 3, testItem3=4;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		testList.PushBack(testItem3);
		int testItem4 = 38;
		bool actualValue = testList.Insert(0, testItem4);
		bool expectedValue = true;
		int actualSize = testList.Size();
		int expectedSize = 4;
		int *actualFront = testList.GetFront();
		int expectedFront = testItem4;

		if (actualValue == expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}

		if (actualFront == nullptr) {
			cout << "Got nullptr for Front" << endl;
			exit(1);
		}

		if (*actualFront == expectedFront) {
			cout << "Front test Pass" << endl;
		}
		else {
			cout << "Front test Fail" << endl;
		}
		
	}	//Test end 4

	{	//Test begin 5
		cout << endl << "Test Insert into back of list" << endl;
		List<int> testList;
		int testItem1 = 2, testItem2 = 3, testItem3 = 4;
		testList.PushBack(testItem1);
		testList.PushBack(testItem2);
		testList.PushBack(testItem3);
		int testItem4 = 38;
		bool actualValue = testList.Insert(testList.Size(), testItem4);
		bool expectedValue = true;
		int actualSize = testList.Size();
		int expectedSize = 4;
		int *actualBack = testList.GetBack();
		int expectedBack = testItem4;

		if (actualValue == expectedValue) {
			cout << "Value test Pass" << endl;
		}
		else {
			cout << "Value test Fail" << endl;
		}

		if (actualSize == expectedSize) {
			cout << "Size test Pass" << endl;
		}
		else {
			cout << "Size test Fail" << endl;
		}

		if (actualBack == nullptr) {
			cout << "Got nullptr for Back" << endl;
			exit(1);
		}

		if (*actualBack == expectedBack) {
			cout << "Back test Pass" << endl;
		}
		else {
			cout << "Back test Fail" << endl;
		}
	}	//Test end 5
}
