// Jeremiah Cooley
// Project 1
// Test Driver
#include <iostream>
#include <string>
#include "Tester.hpp"

using namespace std;

int main()
{
    Tester tester;
    tester.RunTests();

    return 0;
}


