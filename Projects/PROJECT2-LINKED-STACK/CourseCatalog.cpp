#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"
#include "DATA_STRUCTURES/Stack.hpp" // LinkedStack

CourseCatalog::CourseCatalog()
{
	LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
	Menu::Header("LOADING COURSES");

	ifstream input("courses.txt");

	if (!input.is_open())
	{
		cout << "Error opening input text file, courses.txt" << endl;
		return;
	}

	string label, courseCode, courseName, prerequisite;
	Course newCourse;

	while (input >> label)
	{
		if (label == "COURSE")
		{
			if (newCourse.name != "")
			{
				m_courses.PushBack(newCourse);
				newCourse.Clear();
			}

			input >> newCourse.code >> newCourse.name;
		}
		else if (label == "PREREQ")
		{
			input >> newCourse.prereq;
		}
	}

	input.close();

	cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses() noexcept
{
	Menu::Header("VIEW COURSES");
	cout << "#\tCODE\tTITLE" << endl;
	cout << "\t  PREREQS" << endl;
	Menu::DrawHorizontalBar(80);
	for (size_t i = 0; i < m_courses.Size(); i++) {
		Course tmp = m_courses[i];
		cout << i+1 << "\t" << tmp.code << "\t" << tmp.name << "\t" << tmp.prereq << endl;
		string prereq = tmp.prereq;
		try {
			tmp = FindCourse(prereq);
		}
		catch (CourseNotFound ex) {
			continue;
		}
		cout << "\t  " << tmp.code << endl;
	}
}

Course CourseCatalog::FindCourse(const string& code)
{
	for (size_t i = 0; i < m_courses.Size(); i++) {
		if (m_courses[i].code == code) {
			return m_courses[i];
		}
	}
	throw CourseNotFound("Invalid Course Code");
}

void CourseCatalog::ViewPrereqs() noexcept
{
	Menu::Header("GET PREREQS");
	string courseCode = Menu::GetStringLine("Enter class code");
	Course current;
	try {
		current = FindCourse(courseCode);
	}
	catch (CourseNotFound ex) {
		cout << "Error! Unable to find course " << courseCode << "!" << endl;
		return;
	}

	LinkedStack<Course> prereqs;
	prereqs.Push(current);
	while (true) {
		try {
			current = FindCourse(current.prereq);
			prereqs.Push(current);
		}
		catch (CourseNotFound ex) {
			break;
		}
	}
	cout << "Classes to take:" << endl;
	while (prereqs.Size() > 0) {
		Course course = prereqs.Top();
		size_t i = 1;
		cout << i++ << ".\t" << course.code << "\t" << course.name << endl;
		prereqs.Pop();
	}
	for (size_t i = 0; i < prereqs.Size(); i++) {
		
	}
}

void CourseCatalog::Run()
{
	bool done = false;
	while (!done)
	{
		Menu::Header("MAIN MENU");

		int choice = Menu::ShowIntMenuWithPrompt({ "View all courses", "Get course prerequisites", "Exit" });

		switch (choice)
		{
		case 1:
			ViewCourses();
			break;

		case 2:
			ViewPrereqs();
			break;

		case 3:
			done = true;
			break;
		}
	}
}
