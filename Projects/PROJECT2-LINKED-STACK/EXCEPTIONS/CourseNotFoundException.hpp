#ifndef _COURSE_NOT_FOUND_EXCEPT_HPP
#define _COURSE_NOT_FOUND_EXCEPT_HPP

#include <stdexcept>
using namespace std;

class CourseNotFound : runtime_error {
public:
	CourseNotFound(const string& msg) :runtime_error(msg) {}
};

#endif
