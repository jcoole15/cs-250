#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"

template <typename T>
class LinkedStack
{
    public:
    LinkedStack()
    {
    }

    void Push( const T& newData ) noexcept
    {
		Node<T> *tmp = new Node<T>();
		tmp->data = newData;
		tmp->ptrNext = nullptr;
		if (m_itemCount == 0) {
			tmp->ptrPrev = nullptr;
			m_ptrFirst = tmp;
		}
		else {			
			tmp->ptrPrev = m_ptrLast;
			m_ptrLast->ptrNext = tmp;			
		}
		m_ptrLast = tmp;
		m_itemCount++;
    }

    T& Top()
    {
		if (m_itemCount == 0) { throw runtime_error("Empty Stack"); }
		return m_ptrLast->data;
    }

    void Pop()
    {
		if (m_itemCount == 0) {
			return;
		}
		else if (m_itemCount == 1) {
			delete m_ptrFirst;
			m_ptrFirst = nullptr;
			m_ptrLast = nullptr;
		}
		else {
			Node<T> *tmp = m_ptrLast;
			m_ptrLast = m_ptrLast->ptrPrev;
			delete tmp;
			m_ptrLast->ptrNext = nullptr;
		}
		m_itemCount--;
    }

    size_t Size() noexcept
    {
        return m_itemCount;
    }

	T& operator[](size_t index) {
		if (index < 0 || index >= m_itemCount) {
			throw new out_of_range("Invalid index");
		}
		if (index == 0) {
			return m_ptrFirst->data;
		}
		Node<T> *iter = m_ptrFirst;
		for (size_t i = 1; i < index; i++) {
			iter = iter->ptrNext;
		}
		return iter->data;

	}

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
